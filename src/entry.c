#include <string.h>
#include <stdlib.h>
#include "dynamic_libs/os_functions.h"
#include "dynamic_libs/socket_functions.h"
#include "dynamic_libs/ax_functions.h"
#include "fs/sd_fat_devoptab.h"
#include "common/common.h"
#include "utils/utils.h"
#include "utils/logger.h"
#include "nin.h"
#include "ninl.h"

extern unsigned char out_raw[];
extern unsigned int out_raw_len;
extern unsigned char wwyu_raw[];
extern unsigned int wwyu_raw_len;

typedef struct _ax_buffer_t {
    u16 format;
    u16 loop;
    u32 loop_offset;
    u32 end_pos;
    u32 cur_pos;
    const unsigned char *samples;
} ax_buffer_t;

#define BUFFER_BASE 0xF4000000
#define TV_BUFFER ((unsigned int*)BUFFER_BASE)
#define TV_BUFFER_1 ((unsigned int*)(BUFFER_BASE + (tv_buf_size / 2)))
#define DRC_BUFFER ((unsigned int*)(BUFFER_BASE + tv_buf_size))
#define DRC_BUFFER_1 ((unsigned int*)(BUFFER_BASE + tv_buf_size + (drc_buf_size / 2)))
unsigned int tv_buf_size = 0;
unsigned int drc_buf_size = 0;

int __entry_menu(int argc, char **argv) {
	InitOSFunctionPointers();
	InitSocketFunctionPointers();
	InitAXFunctionPointers();

	log_init("192.168.192.2");
	log_print("Hello World!\n");

	OSScreenInit();

	tv_buf_size = OSScreenGetBufferSizeEx(0);
	drc_buf_size = OSScreenGetBufferSizeEx(1);
	log_printf("TV: 0x%08X; DRC: 0x%08X\n", tv_buf_size, drc_buf_size);

	OSScreenSetBufferEx(0, TV_BUFFER);
	OSScreenSetBufferEx(1, DRC_BUFFER);

	OSScreenClearBufferEx(0, 0);
	OSScreenClearBufferEx(1, 0);

	OSScreenEnableEx(0, 1);
	OSScreenEnableEx(1, 1);

	//Nintendo/Switch logos fade in/out
	unsigned int* cur_buffer = DRC_BUFFER_1;
	int cur_buffer_i = 1;
	for (int anim = 0xFF; anim > 0; anim--) {
		for (int x = 0; x < 248; x++) {
			for (int y = 0; y < 61; y++) {
				unsigned char alpha = Nintendo_logo_current_data[x + y * 248];
				signed int anim_alpha = (int)alpha & 0xFF;
				anim_alpha -= anim;
				if (anim_alpha < 0) anim_alpha = 0;
				unsigned int colour = (anim_alpha << 8) | (anim_alpha << 16) | (anim_alpha << 24);
				cur_buffer[(x + 300) + (y + 209) * 896] = colour;
			}
		}

		DCFlushRange(cur_buffer, drc_buf_size / 2);
		OSScreenFlipBuffersEx(1);

		cur_buffer = (cur_buffer_i) ? DRC_BUFFER : DRC_BUFFER_1;
		cur_buffer_i = !cur_buffer_i;

		usleep(2000);
	}

	sleep(2);

	for (int anim = 0; anim < 0xFF; anim++) {
		for (int x = 0; x < 248; x++) {
			for (int y = 0; y < 61; y++) {
				unsigned char alpha = Nintendo_logo_current_data[x + y * 248];
				signed int anim_alpha = (int)alpha & 0xFF;
				anim_alpha -= anim;
				if (anim_alpha < 0) anim_alpha = 0;
				unsigned int colour = (anim_alpha << 8) | (anim_alpha << 16) | (anim_alpha << 24);
				cur_buffer[(x + 300) + (y + 209) * 896] = colour;
			}
		}

		DCFlushRange(cur_buffer, drc_buf_size / 2);
		OSScreenFlipBuffersEx(1);

		cur_buffer = (cur_buffer_i) ? DRC_BUFFER : DRC_BUFFER_1;
		cur_buffer_i = !cur_buffer_i;

		usleep(2000);
	}

	for (int anim = 0xFF; anim > 0; anim--) {
		for (int x = 0; x < 246; x++) {
			for (int y = 0; y < 250; y++) {
				unsigned char alpha = Nintendo_Switch_Logo_black_data[x + y * 246];
				signed int anim_alpha = (int)alpha & 0xFF;
				anim_alpha -= anim;
				if (anim_alpha < 0) anim_alpha = 0;
				unsigned int colour = (anim_alpha << 8) | (anim_alpha << 16) | (anim_alpha << 24);
				cur_buffer[(x + 303) + (y + 114) * 896] = colour;
			}
		}

		DCFlushRange(cur_buffer, drc_buf_size / 2);
		OSScreenFlipBuffersEx(1);

		cur_buffer = (cur_buffer_i) ? DRC_BUFFER : DRC_BUFFER_1;
		cur_buffer_i = !cur_buffer_i;

		usleep(2000);
	}

	sleep(2);

	for (int anim = 0; anim < 0xFF; anim++) {
		for (int x = 0; x < 246; x++) {
			for (int y = 0; y < 250; y++) {
				unsigned char alpha = Nintendo_Switch_Logo_black_data[x + y * 246];
				signed int anim_alpha = (int)alpha & 0xFF;
				anim_alpha -= anim;
				if (anim_alpha < 0) anim_alpha = 0;
				unsigned int colour = (anim_alpha << 8) | (anim_alpha << 16) | (anim_alpha << 24);
				cur_buffer[(x + 303) + (y + 114) * 896] = colour;
			}
		}

		DCFlushRange(cur_buffer, drc_buf_size / 2);
		OSScreenFlipBuffersEx(1);

		cur_buffer = (cur_buffer_i) ? DRC_BUFFER : DRC_BUFFER_1;
		cur_buffer_i = !cur_buffer_i;

		usleep(2000);
	}

	void* audio = malloc(wwyu_raw_len);
	memcpy(audio, wwyu_raw, wwyu_raw_len);

	DCFlushRange(audio, wwyu_raw_len);

	unsigned int params[3] = {1, 0, 0};
	AXInitWithParams(params);
	void* voice = AXAcquireVoice(25, 0, 0);
	AXVoiceBegin(voice);
	AXSetVoiceType(voice, 0);
	unsigned int vol = 0x80000000;
	AXSetVoiceVe(voice, &vol);
	unsigned int mix[24];
	memset(mix, 0, sizeof(mix));
	mix[0] = vol;
	mix[4] = vol;
	AXSetVoiceDeviceMix(voice, 0, 0, mix);
	AXSetVoiceDeviceMix(voice, 1, 0, mix);
	AXVoiceEnd(voice);

	ax_buffer_t vBuf;
	memset(&vBuf, 0, sizeof(ax_buffer_t));
	vBuf.samples = audio;
	vBuf.format = 25;
	vBuf.loop = 0;
	vBuf.cur_pos = 0;
	vBuf.end_pos = wwyu_raw_len - 1;
	vBuf.loop_offset = 0;

	unsigned int ratioBits[4];
	ratioBits[0] = (unsigned int)(0x00010000 * ((float)48000 / (float)AXGetInputSamplesPerSec()));
	ratioBits[1] = 0;
	ratioBits[2] = 0;
	ratioBits[3] = 0;

	AXSetVoiceSrc(voice, ratioBits);
	AXSetVoiceSrcType(voice, 1);

	while (1) { //new in 0.2
		AXSetVoiceOffsets(voice, &vBuf);
		AXSetVoiceState(voice, 1);

		unsigned int* video = (unsigned int*)out_raw;
		int frames = 22;

		#define SCALE 2
		#define X_OFFSET 72
		#define Y_OFFSET 37

		for (int frame = 0; frame < frames; frame++) {
			unsigned int* cur_frame = (unsigned int*)((void*)video + (frame * 360 * 203 * 4));
			for (int x = 0; x < 360; x++) {
				for (int y = 0; y < 203; y++) {
					for (int rx = x * SCALE; rx < ((x * SCALE) + SCALE); rx++) {
						for (int ry = y * SCALE; ry < ((y * SCALE) + SCALE); ry++) {
							cur_buffer[rx + X_OFFSET + (ry + Y_OFFSET) * 896] = cur_frame[x + y * 360];
						}
					}
				}
			}

			DCFlushRange(cur_buffer, drc_buf_size / 2);
			OSScreenFlipBuffersEx(1);

			cur_buffer = (cur_buffer_i) ? DRC_BUFFER : DRC_BUFFER_1;
			cur_buffer_i = !cur_buffer_i;

			msleep(93);
		}
		AXSetVoiceState(voice, 0);
	}

	/*log_print("Done.\n");
	sleep(1);

	AXQuit();

	OSScreenClearBufferEx(0, 0);
	OSScreenClearBufferEx(1, 0);

	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	OSScreenClearBufferEx(0, 0);
	OSScreenClearBufferEx(1, 0);

	DCFlushRange((void*)BUFFER_BASE, tv_buf_size + drc_buf_size);

	log_deinit();
	return EXIT_SUCCESS;*/
}
