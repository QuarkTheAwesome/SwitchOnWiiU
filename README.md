## Switch On Wii U
---
### Wouldn't it be cool to have a Nintendo Switch on your Wii U?
> Well now you can!

---
### Wait, what is this?
> pure awesomeness

This piece of software was made by QuarkTheAwesome with a few modifications made by curtisy1. It uses some of the shared hardware between the Switch and the Wii U to boot Switch OS on a Wii U.

---
### Cool, so what does this do?
> Just try it and see for yourself  ( ͡° ͜ʖ ͡°)

After much sweat and tears, we were able to temporarily freeze the IOSU and load Switch OS in its place. Since both the Switch and the IOSU use ARM processors, they are actually compatible and the console boots - as a Switch!

---
### Why release this now all of a sudden?
> It was about time

We had a pretty long and intensive testing phase to make sure everything is working perfectly before releasing. And now we feel that everything is mostly bug-free, time for the world to see!

---
### How large is the brick risk?
> 0%

As I mentioned, this product was tested thoroughly. We even borrowed a monkey from Volkswagen to test it, and they beat SMO using our program, so it must be secure, right?!

---
### Fine. How do I start this
> As you usually do

Just put it in your apps folder and select it from within the Homebrew Launcher. We don't have any logo unfortunately :(

---
### Is this supposed to be a joke?
> absolutely not!

Of course not! We just thought it would be a great opportunity to release this. For those of you who read the code, we actually use a new generation of exploits to gain control of the IOSU - we take hold through a parsing bug and eventual DMA attack via the sound card, and then copy Switch OS in through the framebuffer. That's why the code is so simple - we just have to point the sound card at the exploit data and copy the framebuffer.

---
### I found a bug, where do I report it?
> That's impossible, we used bugspray

---
---

<sup><sup>Happy<sup>April<sup>Fools<sup>Guys!❤️
</sup></sup></sup></sup></sup>